#!/bin/bash

TestPath()
{
	if [[ -e $1 && -d $1 ]]; then
		return 0
	fi

	return 1
}

CheckResult()
{
	if [[ $? -ne 0 ]]; then
		exit $?
	fi
}

TEST_PATH=/opt/steam-runtime
TestPath "$TEST_PATH"
CheckResult

if ! TestPath "runtime/i386"; then
	[[ "$(dpkg --print-architecture)" == "i386" ]] || ! TestPath "runtime/amd64"
	CheckResult

	if [[ -z "$STEAM_RUNTIME_ROOT" ]]; then
		$TEST_PATH/shell.sh --arch=i386
		CheckResult
	fi

	[[ -n "STEAM_RUNTIME_ROOT" && "$STEAM_RUNTIME_TARGET_ARCH" == "i386" ]]
	CheckResult
fi

if ! TestPath "runtime/amd64"; then
	[[ "$(dpkg --print-architecture)" == "amd64" ]] || ! TestPath "runtime/i386"
	CheckResult

	if [[ -z "$STEAM_RUNTIME_ROOT" ]]; then
		$TEST_PATH/shell.sh --arch=amd64
		CheckResult
	fi

	[[ -n "STEAM_RUNTIME_ROOT" && "$STEAM_RUNTIME_TARGET_ARCH" == "amd64" ]]
	CheckResult
fi
